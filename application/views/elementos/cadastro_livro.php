<div class="container col-md-5 mx-auto">
    <form class="border border-light p-5" method="post">

        <p class="h4 mb-4 text-center">Cadastrar livro</p>

        <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome do livro">

        <input type="text" name="autor" id="autor" class="form-control" placeholder="Autor">
        
        <input type="text" name="genero" id="genero" class="form-control" placeholder="Gênero">

        <input type="text" name="ano" id="ano" class="form-control" placeholder="Ano de Lançamento">
        
        <input type="text" name="idioma" id="idioma" class="form-control" placeholder="Idioma">

        <input type="text" name="capa" id="capa" class="form-control" value="../../assets/img/*.jpg" placeholder="Capa">
        
        <input type="text" name="descr" id="descr" class="form-control" placeholder="Descrição">
        
        <input type="text" name="preco" id="preco" class="form-control" placeholder="Preço">

        <button class="btn btn-info my-4 btn-block" type="submit">Cadastrar</button>

    </form>
</div>