<div class="container">
<div class="row">
        <div class='col-md-12 mx-auto'>
            <h2 class="h1-responsive font-weight-bold text-center my-5">Contato</h2>
        </div>
    </div>
    <div class="row">
        <div class='col-md-12 mx-auto'>
            <?= $contato ?>
        </div>
    </div>
    <div class="row">
        <div class='col-md-12 mx-auto'>
            <?= $mapa ?>
        </div>
    </div>
</div>