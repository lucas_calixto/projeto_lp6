<div class="bg"></div>
<div class="row">
    <div class="col-md-12">
        <?= $carrossel ?>
    </div>
</div>
<div class="container mx-auto mt-4 text-center wow fadeInUp">
    <div class="row">
        <div class="col-md-12">
            <?= $titulo ?>
            <?= $texto ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $card_group ?>
        </div>
    </div>
</div>